<?php

    include "registros.php";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Programa de las olimpiadas</title>





    <!-- CSS 
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    -->
    <!-- CSS 4.0 -->
    <link rel="stylesheet" href="css/bootstrap.min.css" crossorigin="anonymous">

    <!-- JS   
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js" integrity="sha384-eMNCOe7tC1doHpGoWe/6oMVemdAVTMs2xqW4mwXrXsW0L84Iytr2wi5v2QjrP/xp" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js" integrity="sha384-cn7l7gDp0eyniUwwAZgrzD06kc/tftFf19TOAs2zVinnD/C7E91j9yyk5//jjpt/" crossorigin="anonymous"></script>
    -->  
     <!-- JS 4.0 -->
    <script src="js/jquery-3.5.1.slim.min.js" ></script>
    <script src="js/popper.min.js" ></script>
    <script src="js/bootstrap.min.js"></script>



</head>
<body>



<img src="../Imagenes/IMAGENOLIMPIADAS.png">

    <div class="card-header bg-dark text-left">
     <!-- Button trigger modal -->
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
        Registrar Institucion +
        </button>   
    </div>


<div class="container">
    <form action="" method="post" ectype="multipart/form-data">
 



        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Registro de instituciones</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                    
                    <input type="hidden" name="txtid" required value="<?php echo $txtid;?>" placeholder="" id="txtid" requiere="">
                    
                    <div class="row align-items-start"> 
                        <div class="form-group col-md-4">
                        <label for="">Nombre:</label>
                        <input type="text" class="form-control <?php echo (isset($error['nombre']))?"is-invalid":"";?>" name="txtnombre" value="<?php echo $txtnombre;?>" placeholder="" id="txtnombre" requiere="">
                        <div class="invalid-feedback">
                        <?php echo (isset($error['nombre']))?$error['nombre']:"";?>
                        </div>
                        <br>
                        </div>

                        <div class="form-group col-md-4">
                        <label for="">Direccion:</label>
                        <input type="text" class="form-control <?php echo (isset($error['direccion']))?"is-invalid":"";?>" name="txtdireccion" value="<?php echo $txtdireccion;?>" placeholder="" id="txtdireccion" requiere="">
                        <div class="invalid-feedback">
                        <?php echo (isset($error['direccion']))?$error['direccion']:"";?>
                        </div>
                        <br>
                        </div>

                        <div class="form-group col-md-4">
                        <label for="">Contacto:</label>
                        <input type="text" class="form-control <?php echo (isset($error['contacto']))?"is-invalid":"";?>" name="txtcontacto" value="<?php echo $txtcontacto;?>" placeholder="" id="txtcontacto" requiere="">
                        <div class="invalid-feedback">
                        <?php echo (isset($error['contacto']))?$error['contacto']:"";?>
                        </div>
                        <br>
                        </div>
                    </div>

                    <div class="row align-items-start"> 
                        <div class="form-group col-md-6">
                        <label for="">Correo electronico:</label>
                        <input type="text" class="form-control <?php echo (isset($error['email']))?"is-invalid":"";?>" name="txtemail" value="<?php echo $txtemail;?>" placeholder="" id="txtemail" requiere="">
                        <div class="invalid-feedback">
                        <?php echo (isset($error['email']))?$error['email']:"";?>
                        </div>
                        <br>
                        </div>
                        
                        <div class="form-group col-md-6">
                        <label for="">Latitud:</label>
                        <input type="text" class="form-control <?php echo (isset($error['latitud']))?"is-invalid":"";?>" name="txtlatitud" value="<?php echo $txtlatitud;?>" placeholder="" id="txtlatitud" requiere="">
                        <div class="invalid-feedback">
                        <?php echo (isset($error['latitud']))?$error['latitud']:"";?>
                        </div>
                        <br>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                    <label for="">Longitud:</label>
                    <input type="text" class="form-control <?php echo (isset($error['longitud']))?"is-invalid":"";?>" name="txtlongitud" value="<?php echo $txtlongitud;?>" placeholder="" id="txtlongitud" requiere="">
                    <div class="invalid-feedback">
                        <?php echo (isset($error['longitud']))?$error['longitud']:"";?>
                        </div>
                    <br>
                    </div>

                    <div class="form-group col-md-12">
                    <label for="">Estado:</label>
                    <input type="number" class="form-control <?php echo (isset($error['estado']))?"is-invalid":"";?>" name="txtestado" value="<?php echo $txtestado;?>" placeholder="" id="txtestado" requiere="">
                    <div class="invalid-feedback">
                        <?php echo (isset($error['estado']))?$error['estado']:"";?>
                        </div>
                    <br>
                    </div>

                    </div>
                </div>
                <div class="modal-footer">
                    
                    <button class="btn btn-success" <?php echo $accionAgregar;?> value="btnAgregar" type="submit" name="accion">Agregar</button>
                    <button class="btn btn-warning" <?php echo $accionModificar;?> value="btnModificar" type="submit" name="accion">Modificar</button>
                    <button class="btn btn-danger" onclick="return Confirmar('¿Realmente deseas borrar el registro?');" <?php echo $accionEliminar;?> value="btnEliminar" type="submit" name="accion">Eliminar</button>
                    <button class="btn btn-primary" <?php echo $accionCancelar;?> value="btnCancelar" type="submit" name="accion">Cancelar</button>
                
                </div>
                </div>
            </div>
        </div>

</br>




    <br>
    <br>


    </form>

        <div class="row">

            <table class="table table-hover table-bordered">
                <thead class="thead-dark">
                    <tr>
                    <th>ID</th>
                    <th>Institucion</th>
                    <th>Direccion</th>
                    <th>Contacto</th>
                    <th>Correo electronico</th>
                    <th>Latitud</th>
                    <th>Longitud</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                    </tr>
                </thead>

            <?php foreach($listaRegistros as $registro){ ?>

                <tr class="bg-light">
                    
                    <td class="text-dark"><?php echo $registro['id']; ?></td>
                    <td class="text-dark"><?php echo $registro['nombre']; ?></td>
                    <td class="text-dark"><?php echo $registro['direccion']; ?></td>
                    <td class="text-dark"><?php echo $registro['contacto']; ?></td>
                    <td class="text-dark"><?php echo $registro['email']; ?></td>
                    <td class="text-dark"><?php echo $registro['latitud']; ?></td>
                    <td class="text-dark"><?php echo $registro['longitud']; ?></td>
                    <td class="text-dark"><?php echo $registro['estado']; ?></td>
                    
                    <td>
                        
                    <form action="" method="post">


                        <input type="hidden" name="txtid" value="<?php echo $registro['id']; ?>">
                        <input type="hidden" name="txtnombre" value="<?php echo $registro['nombre']; ?>">
                        <input type="hidden" name="txtdireccion" value="<?php echo $registro['direccion']; ?>">
                        <input type="hidden" name="txtcontacto" value="<?php echo $registro['contacto']; ?>">
                        <input type="hidden" name="txtemail" value="<?php echo $registro['email']; ?>">
                        <input type="hidden" name="txtlatitud" value="<?php echo $registro['latitud']; ?>">
                        <input type="hidden" name="txtlongitud" value="<?php echo $registro['longitud']; ?>">
                        <input type="hidden" name="txtestado" value="<?php echo $registro['estado']; ?>">

                        <input type="submit" value="Seleccionar" class="btn btn-info" name="accion">

                        <button value="btnEliminar" onclick="return Confirmar('¿Realmente deseas borrar el registro?');" type="submit" class="btn btn-danger" name="accion">Eliminar</button>

                        <a type="submit" value=" <?php $txtid?> " class="btn btn-success" href="personas.php?id=<?php echo $registro['id']; ?>" name="btnPersonas">Personas</a>

                    </form>
                    

                    </td>

                </tr>

            <?php } ?>

            </table>

        </div>

    <?php if($mostrarModal){?>

        <script>
            $('#exampleModal').modal('show');
        </script>

    <?php }?>


<script>
    function Confirmar(Mensaje){
        return (confirm(Mensaje))?true:false;

    }
    $(document).ready(function()
    {   $("#exampleModal").click(function()
        {
            //alert('hola');
            $('#exampleModal').modal('show');
        })
        //$('#exampleModal').modal('show');
    });
</script>



</div>    
</body>
</html>