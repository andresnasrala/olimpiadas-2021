
const int  pulsador_asc = 2;
const int  pulsador_des = 3;
const int led_rojo = 6;
const int led_verde = 5;
const int buzz = 7;
int contador            = 0;
int estado_pulsador_asc = 0;
int lastButtonState_asc = 1;
int estado_pulsador_des = 0;
int lastButtonState_des = 1;
#include <SPI.h>
#include <Ethernet.h>


byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 1, 200);


EthernetServer server(80);

void setup() {}


  Serial.begin(9600);
  while (!Serial) {

  }
  Serial.println("Ethernet WebServer Example");


  Ethernet.begin(mac, ip);

  if (Ethernet.hardwareStatus() == EthernetNoHardware) {
    Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
    while (true) {
      delay(1); 
    }
  }
  if (Ethernet.linkStatus() == LinkOFF) {
    Serial.println("Ethernet cable is not connected.");
  }

  // start the server
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());
  Serial.begin(9600);

  pinMode(pulsador_asc, INPUT_PULLUP); 
  pinMode(pulsador_des, INPUT_PULLUP); 
  pinMode(led_rojo, OUTPUT);
  pinMode (led_verde, OUTPUT);
  pinMode (buzz, OUTPUT);
}


void loop() {

// listen for incoming clients
  EthernetClient client = server.available();
  if (client) {
    Serial.println("new client");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        Serial.write(c);

      
        }
      }
    }
}
    delay(1);

    client.stop();
    Serial.println("client disconnected");
  }
  estado_pulsador_asc = digitalRead(pulsador_asc);
  estado_pulsador_des = digitalRead(pulsador_des);


  if (estado_pulsador_asc != lastButtonState_asc) {

    if (estado_pulsador_asc == HIGH) {

      contador++;
      Serial.println("+");
      Serial.print("personas en tiempo real numero de personas acumuladas ");
      Serial.println(contador);
      delay(100);
    }
  }
  lastButtonState_asc  = estado_pulsador_asc;
  if (estado_pulsador_des != lastButtonState_des ) {

    if (estado_pulsador_des == HIGH) {

      contador--;
      Serial.println("-");
      Serial.print("personas en tiempo real numero de personas acumuladas ");
      Serial.println(contador);
      delay(100);
    }
  }

  lastButtonState_des  = estado_pulsador_des;


  if (contador >= 50) {
    digitalWrite(led_rojo, HIGH);
    digitalWrite(led_verde, false);
    digitalWrite (buzz, HIGH);
    delay(50);
    digitalWrite(buzz, LOW);
    string PATH_INGRESAR = "/ingresar.php?
    idinstitucion=1

    if (contador <= 50) {
      digitalWrite (led_rojo, true);
      digitalWrite (led_verde, LOW);
      string PATH_EGRESAR = "/egresar.php?
      idinstitucion=1
    }
  }

  }
