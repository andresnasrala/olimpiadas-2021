-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 26-08-2021 a las 00:05:27
-- Versión del servidor: 5.7.31
-- Versión de PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `olimpiadas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `egresos`
--

DROP TABLE IF EXISTS `egresos`;
CREATE TABLE IF NOT EXISTS `egresos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_institucion` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `egresos`
--

INSERT INTO `egresos` (`id`, `fecha_hora`, `id_institucion`) VALUES
(1, '2021-08-10 11:39:41', 1),
(2, '2021-08-10 13:05:48', 1),
(3, '2021-08-17 00:38:13', 1),
(4, '2021-08-21 22:36:04', 1),
(5, '2021-08-22 14:04:10', 1),
(6, '2021-08-23 00:06:49', 1),
(7, '2021-08-23 00:06:49', 1),
(8, '2021-08-23 00:06:49', 1),
(9, '2021-08-23 00:06:49', 1),
(10, '2021-08-23 00:12:44', 1),
(11, '2021-08-23 00:12:44', 1),
(12, '2021-08-23 00:12:44', 1),
(13, '2021-08-23 00:12:44', 1),
(14, '2021-08-23 00:12:44', 1),
(15, '2021-08-23 00:12:44', 1),
(16, '2021-08-23 02:09:05', 1),
(17, '2021-08-23 02:09:27', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingresos`
--

DROP TABLE IF EXISTS `ingresos`;
CREATE TABLE IF NOT EXISTS `ingresos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_institucion` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ingresos`
--

INSERT INTO `ingresos` (`id`, `fecha_hora`, `id_institucion`) VALUES
(1, '2021-08-10 11:33:17', 1),
(2, '2021-08-10 11:33:23', 2),
(3, '2021-08-10 11:33:26', 1),
(4, '2021-08-10 13:05:13', 1),
(5, '2021-08-10 13:05:32', 1),
(6, '2021-08-17 00:18:28', 1),
(7, '2021-08-17 00:18:28', 2),
(8, '2021-08-17 00:18:47', 1),
(9, '2021-08-17 00:18:47', 1),
(10, '2021-08-21 22:35:36', 1),
(11, '2021-08-21 22:35:36', 1),
(12, '2021-08-22 14:03:53', 1),
(13, '2021-08-22 14:03:53', 1),
(14, '2021-08-22 14:57:44', 1),
(15, '2021-08-22 14:57:44', 1),
(16, '2021-08-22 14:57:56', 1),
(17, '2021-08-22 23:52:32', 1),
(18, '2021-08-22 23:53:41', 1),
(19, '2021-08-22 23:54:49', 1),
(20, '2021-08-23 00:03:31', 1),
(21, '2021-08-23 00:03:31', 1),
(22, '2021-08-23 00:03:31', 1),
(23, '2021-08-23 00:03:31', 1),
(24, '2021-08-23 00:03:31', 1),
(25, '2021-08-23 00:03:31', 1),
(26, '2021-08-23 00:03:31', 1),
(27, '2021-08-23 00:03:31', 1),
(28, '2021-08-23 00:03:31', 1),
(29, '2021-08-23 00:03:31', 1),
(30, '2021-08-23 00:03:31', 1),
(31, '2021-08-23 00:03:31', 1),
(32, '2021-08-23 00:03:31', 1),
(33, '2021-08-23 00:03:31', 1),
(34, '2021-08-23 00:03:31', 1),
(35, '2021-08-23 00:03:31', 1),
(36, '2021-08-23 00:03:31', 1),
(37, '2021-08-23 00:03:31', 1),
(38, '2021-08-23 00:03:31', 1),
(39, '2021-08-23 00:03:31', 1),
(40, '2021-08-23 00:03:31', 1),
(41, '2021-08-23 00:03:31', 1),
(42, '2021-08-23 00:03:31', 1),
(43, '2021-08-23 00:03:31', 1),
(44, '2021-08-23 00:03:31', 1),
(45, '2021-08-23 00:03:31', 1),
(46, '2021-08-23 00:03:31', 1),
(47, '2021-08-23 00:03:31', 1),
(48, '2021-08-23 00:03:31', 1),
(49, '2021-08-23 00:03:31', 1),
(50, '2021-08-23 00:03:31', 1),
(51, '2021-08-23 00:03:31', 1),
(52, '2021-08-23 00:03:31', 1),
(53, '2021-08-23 00:03:31', 1),
(54, '2021-08-23 00:03:31', 1),
(55, '2021-08-23 00:03:31', 1),
(56, '2021-08-23 00:03:31', 1),
(57, '2021-08-23 00:03:31', 1),
(58, '2021-08-23 00:03:31', 1),
(59, '2021-08-23 00:03:31', 1),
(60, '2021-08-23 00:03:31', 1),
(61, '2021-08-23 00:03:31', 1),
(62, '2021-08-23 00:03:31', 1),
(63, '2021-08-23 00:03:31', 1),
(64, '2021-08-23 00:11:51', 1),
(65, '2021-08-23 00:11:51', 1),
(66, '2021-08-23 00:11:51', 1),
(67, '2021-08-23 00:11:51', 1),
(68, '2021-08-23 00:11:51', 1),
(69, '2021-08-23 01:33:01', 1),
(70, '2021-08-23 02:03:50', 1),
(71, '2021-08-23 02:03:50', 1),
(72, '2021-08-23 02:03:50', 1),
(73, '2021-08-23 02:04:22', 1),
(74, '2021-08-24 00:10:00', 1),
(75, '2021-08-24 00:10:00', 1),
(76, '2021-08-25 20:01:51', 1),
(77, '2021-08-25 20:01:51', 1),
(78, '2021-08-25 20:01:51', 1),
(79, '2021-08-25 20:01:51', 1),
(80, '2021-08-25 20:01:51', 1),
(81, '2021-08-25 20:01:51', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instituciones`
--

DROP TABLE IF EXISTS `instituciones`;
CREATE TABLE IF NOT EXISTS `instituciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text NOT NULL,
  `direccion` text NOT NULL,
  `contacto` text,
  `email` text,
  `latitud` text NOT NULL,
  `longitud` text NOT NULL,
  `estado` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `instituciones`
--

INSERT INTO `instituciones` (`id`, `nombre`, `direccion`, `contacto`, `email`, `latitud`, `longitud`, `estado`) VALUES
(1, 'Carrefour Market San Juan', 'Gral. Mariano Acha Sur 32, J5400 San Juan', '+548004448484', '', '-31.533284038694852', '-68.52494350786384', 1),
(2, 'Sanatorio Argentino Consultorios Externos', 'Sta Fe Este 263, J5402 AAE, San Juan', '+542644303050', NULL, '-31.538420579014556', '-68.52187608249328', 1),
(3, 'Despensa La familia', 'casa123', '2645758661', 'lafamilia@gmail.com', 'latitud', 'longitud', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE IF NOT EXISTS `login` (
  `usuario` text NOT NULL,
  `password` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`usuario`, `password`) VALUES
('biblioteca123', 'biblioteca123');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
