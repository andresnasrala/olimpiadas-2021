<?php


$txtid=(isset($_POST['txtid']))?$_POST['txtid']:"";
$txtnombre=(isset($_POST['txtnombre']))?$_POST['txtnombre']:"";
$txtdireccion=(isset($_POST['txtdireccion']))?$_POST['txtdireccion']:"";
$txtcontacto=(isset($_POST['txtcontacto']))?$_POST['txtcontacto']:"";
$txtemail=(isset($_POST['txtemail']))?$_POST['txtemail']:"";
$txtlatitud=(isset($_POST['txtlatitud']))?$_POST['txtlatitud']:"";
$txtlongitud=(isset($_POST['txtlongitud']))?$_POST['txtlongitud']:"";
$txtestado=(isset($_POST['txtestado']))?$_POST['txtestado']:"";

$accion=(isset($_POST['accion']))?$_POST['accion']:"";


$error=array();


$accionAgregar="";
$accionModificar=$accionEliminar=$accionCancelar="disabled";
$mostrarModal=false;


include ("conexion.php");

switch($accion){
    case "btnAgregar":

            if($txtnombre==""){
                $error['nombre']="¡Complete el campo!";
            }
            if($txtdireccion==""){
                $error['direccion']="¡Complete el campo!";
            }
            if($txtcontacto==""){
                $error['contacto']="¡Complete el campo!";
            }
            if($txtlatitud==""){
                $error['latitud']="¡Complete el campo!";
            }
            if($txtlongitud==""){
                $error['longitud']="¡Complete el campo!";
            }
            if($txtestado==""){
                $error['estado']="¡Complete el campo!";
            }

            if(count($error)>0){
                $mostrarModal=true;
                break;
            }


            $sentencia=$pdo->prepare("INSERT INTO instituciones(nombre,direccion,contacto,email,latitud,longitud,estado)
            VALUES (:nombre,:direccion,:contacto,:email,:latitud,:longitud,:estado) ");

            $sentencia->bindParam(':nombre',$txtnombre);
            $sentencia->bindParam(':direccion',$txtdireccion);
            $sentencia->bindParam(':contacto',$txtcontacto);
            $sentencia->bindParam(':email',$txtemail);
            $sentencia->bindParam(':latitud',$txtlatitud);
            $sentencia->bindParam(':longitud',$txtlongitud);
            $sentencia->bindParam(':estado',$txtestado);
            $sentencia->execute();


            header('Location: instituciones.php');

    break;
    case "btnModificar":


        $sentencia=$pdo->prepare("UPDATE instituciones SET
        nombre=:nombre,
        direccion=:direccion,
        contacto=:contacto,
        email=:email,
        latitud=:latitud,
        longitud=:longitud,
        estado=:estado WHERE
        id=:id");


        $sentencia->bindParam(':nombre',$txtnombre);
        $sentencia->bindParam(':direccion',$txtdireccion);
        $sentencia->bindParam(':contacto',$txtcontacto);
        $sentencia->bindParam(':email',$txtemail);
        $sentencia->bindParam(':latitud',$txtlatitud);
        $sentencia->bindParam(':longitud',$txtlongitud);
        $sentencia->bindParam(':estado',$txtestado);
        $sentencia->bindParam(':id',$txtid);
        $sentencia->execute();

        header('Location: instituciones.php');


    break;
    case "btnEliminar":

        $sentencia=$pdo->prepare(" DELETE FROM instituciones WHERE id=:id");

        $sentencia->bindParam(':id',$txtid);
        $sentencia->execute();

        header('Location: instituciones.php');

    break;
    case "btnCancelar":
        header('Location: instituciones.php');
    break;
    case "Seleccionar":

        $accionAgregar="disabled";
        $accionModificar=$accionEliminar=$accionCancelar="";
        $mostrarModal=true;

    break;
}

    $sentencia= $pdo->prepare("SELECT * FROM `instituciones` where estado = 1");
    $sentencia->execute();
    $listaRegistros=$sentencia->fetchAll(PDO::FETCH_ASSOC);




?>