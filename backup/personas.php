<!-- CODIGO PHP DE PERSONAS -->

<?php 

include ("conexion.php");
include ("registros.php");
include('header.php');
include('menu.php');


$idinstitucion=(isset($_GET['id']))?$_GET['id']:"";

//id_instituciones=1

//SELECT * FROM `ingresos` WHERE `id_institucion`=1
$fechaHora = date("Y-m-d H:i:s");
$fechaHoyMin = date("Y-m-d").' 00:00:00';
$fechaHoyMax = date("Y-m-d").' 23:59:59';
$ingresaron = "SELECT count(id_institucion) as contador FROM `ingresos` where id_institucion = ".$idinstitucion." and fecha_hora > '$fechaHoyMin' and fecha_hora < '$fechaHoyMax'";
//echo $ingresaron."<br>";
$egresaron="SELECT count(id_institucion) as contador FROM `egresos` where id_institucion = ".$idinstitucion." and fecha_hora > '$fechaHoyMin' and fecha_hora < '$fechaHoyMax'";

$sentencia= $pdo->prepare($ingresaron);
$sentencia->execute();
$listaIngresos=$sentencia->fetchAll(PDO::FETCH_ASSOC);

$sentencia= $pdo->prepare($egresaron);
$sentencia->execute();
$listaEgresos=$sentencia->fetchAll(PDO::FETCH_ASSOC);


//print_r($listaIngresos[0]["contador"]);

$ingresaron = $listaIngresos[0]["contador"];
$egresaron = $listaEgresos[0]["contador"];

$total=$ingresaron-$egresaron;

?>


<!-- ICONOS -->
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>

<!-- ACA COMIENZA EL CODIGO DE LA PAGINA WEB -->


    <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <script src="http://code.highcharts.com/modules/exporting.js"></script>

<!-- Content Wrapper. contiene el contenido de la pagina -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">CONTROL DE PERSONAS</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    	<div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header" style="background-color: #FF7F00;">
                        <h3 class="card-title">OLIMPIADAS DE PROGRAMACIÓN</h3> 
                        <ol class="float-sm-right">
                            <a class="btn" href="index.php" style="background-color:#000000;">Formulario de Instituciones</a>
                        </ol>
                    </div>
                    <!-- /.card-header -->
                    <br>
                    <!-- form start -->
                    <section class="content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-3 col-6">
                                    <!-- small box -->
                                    <div class="small-box bg-success">
                                        <div class="inner">
                                            
                                            <h3><?php echo $ingresaron." Personas"; ?></h3>

                                            <p>Ingresaron</p>
                                        </div>
                                        <div class="icon">
                                            <i class="ion ion-person-add"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-6">
                                    <!-- small box -->
                                    <div class="small-box bg-danger">
                                        <div class="inner">
                                            
                                            <h3><?php echo $ingresaron." Personas"; ?></h3>

                                            <p>Egresaron</p>
                                        </div>
                                        <div class="icon">
                                            <i class="ion ion-pie-graph"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-6">
                                    <!-- small box -->
                                    <div class="small-box bg-info">
                                        <div class="inner">
                                            
                                            <h3><?php echo $ingresaron." Personas"; ?></h3>

                                            <p>Personas dentro</p>
                                        </div>
                                        <div class="icon">
                                            <i class="ion ion-stats-bars"></i>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                </div>
                <!-- /.card -->
            </div>
        </div>


    </section>
    
    <!-- /.content -->



    <div id="grafica"></div>
    
    <script>
     $(function($){
         $('#grafica').highcharts({
             title:{text:'Promedio de ingresos de la semana'},
             xAxis:{categories:['Lun','Mar','Mier','Jue','Vie','Sab','Dom']},
             yAxis:{title:'Porcentaje %',plotLines:[{value:0,width:1,color:'#808080'}]},
             tooltip:{valueSuffix:''},
             legend:{layout:'vertical',align:'right',verticalAlign:'middle',borderWidth:0},
             series:[{type: 'column',name: 'Ingresos',data: ['Lun','Mar', 21]},
           ],
             plotOptions:{line:{dataLabels:{enabled:true}}}
         });
     });
    </script>




<?php include('footer.php');?>