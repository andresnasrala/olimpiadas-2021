
<!DOCTYPE html>
<html>
<head>
	<title>Biblioteca</title>

	<!-- JQUERY -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!--FRAMEWORK BOOTSTRAP para el estilo de la pagina-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

	<!-- Los iconos tipo Solid de Fontawesome-->
	<lin krel="stylesheet"href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
	<script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>

	<!-- Nuestro css-->
	<link rel="stylesheet"type="text/css"href="css/login.css">
</head>
<body>
	<div class="modal-dialog text-center">
		<div class="col-sm-8 main-section">
			<div class="modal-content">
				<div class="col-12 user-img">
					<img src="imagenes/user.png">
				</div>
				<div class="card-header text-white">Biblioteca E.P.E.T. 4</div>	
					
					<!--Formulario que ingresa datos-->
					<form class="col-12" action="login_usuario_be.php" method="POST">

						<div class="form-group" id="usuario"> <!--Ingresa el usuario-->
							<input type="text" class="form-control" placeholder="Ingrese el usuario" name="usuario">
						</div>

						<div class="form-group" id="contrasena"> <!--Ingresa la contraseña-->
							<input type="text" class="form-control" placeholder="Ingrese la contraseña" name="contrasena">
						</div>

						<button type="submit" class="btn btn-primary" action="login_usuario_be.php" method="POST"><i class="fas fa-sign-in-alt"></i> Iniciar sesion</button>
						<br>
						<br>
					</form>
			</div>
		</div>
	</div>	
</body>
</html>