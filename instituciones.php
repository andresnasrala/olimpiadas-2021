<?php 

include ('conexion.php');
include ('registros.php');
include('header.php');
include('menu.php');


$idinstitucion=(isset($_GET['id']))?$_GET['id']:"";

//id_instituciones=1

//SELECT * FROM `ingresos` WHERE `id_institucion`=1
$fechaHora = date("Y-m-d H:i:s");
$fechaHoyMin = date("Y-m-d").' 00:00:00';
$fechaHoyMax = date("Y-m-d").' 23:59:59';
$ingresaron = "SELECT count(id_institucion) as contador FROM `ingresos` where id_institucion = ".$idinstitucion." and fecha_hora > '$fechaHoyMin' and fecha_hora < '$fechaHoyMax'";
//echo $ingresaron."<br>";
$egresaron="SELECT count(id_institucion) as contador FROM `egresos` where id_institucion = ".$idinstitucion." and fecha_hora > '$fechaHoyMin' and fecha_hora < '$fechaHoyMax'";

$sentencia= $pdo->prepare($ingresaron);
$sentencia->execute();
$listaIngresos=$sentencia->fetchAll(PDO::FETCH_ASSOC);

$sentencia= $pdo->prepare($egresaron);
$sentencia->execute();
$listaEgresos=$sentencia->fetchAll(PDO::FETCH_ASSOC);


//print_r($listaIngresos[0]["contador"]);

$total = 0;
$ingresaron = 0;
$egresaron = 0;

if(isset($listaIngresos[0]["contador"])){

  $ingresaron = $listaIngresos[0]["contador"];

  }

if(isset($listaEgresos[0]["contador"])){

  $egresaron = $listaEgresos[0]["contador"];
  
  }
$total=$ingresaron-$egresaron;

/* aca la produccion*/
?>
    
<!-- Content Wrapper. Contains page content -->

 <div class="content-wrapper" style="background-color: #1c1c1c;">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-white" id="fechaSting">ESTADISTICAS</h1> 
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->



    <!-- Main content -->

  <! -- CUADROS DE ESTADISTICAS -->
    <section class="content">
    	<div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->

                <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box text-white" style="background-color: #00FF00;" id="totalColor">
              <div class="inner">
                <h3 id='total'>0</h3>

                <p>Personas en sitio</p>
              </div>
              <div class="icon">
                <i class="ion ion-location"></i>
              </div>
            </div>
          </div>
          
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box text-white" id="ingresaronColor" style="background-color: #FF7F00;">
              <div class="inner">
                <h3 id='ingresaron'>0<sup style="font-size: 20px"></sup></h3>

                <p>Ingreso de Personas</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box text-white" id="egresaronColor" style="background-color: #663399">
              <div class="inner">
                <h3 id='egresaron'>0</h3>

                <p>Egreso de Personas</p>
              </div>
              <div class="icon">
                <i class="ion ion-person"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info" id="promedioColor ">
              <div class="inner">
                <h3 id='posicion'>0</h3>

                <p>Horario mas concurrido del día</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>

                <div class="card card-primary">
                    <div class="card-header" style="background-color: #FF7F00;">
                        <h3 class="card-title" >Olimpiadas de Programación</h3> 
                        <ol class="float-sm-right">
                            <button type="button" class="btn text-white" data-toggle="modal" data-target="#exampleModal" style="background-color:#000000;">
                            Registrar Institucion +
                            </button>   
                        </ol>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <div class="card-body" style="background-color: #1c1c1c;">
                    

    

                        <div class="row">

                            <table class="table table-hover table-bordered">
                                <thead class="thead-dark">
                                    <tr>
                                    <th>ID</th>
                                    <th>Institucion</th>
                                    <th>Direccion</th>
                                    <th>Contacto</th>
                                    <th>Correo electronico</th>
                                    <th>Latitud</th>
                                    <th>Longitud</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                    </tr>
                                </thead>

                            <?php foreach($listaRegistros as $registro){ ?>

                                <tr class="bg-dark">
                                    
                                    <td class="text-white"><?php echo $registro['id']; ?></td>
                                    <td class="text-white"><?php echo $registro['nombre']; ?></td>
                                    <td class="text-white"><?php echo $registro['direccion']; ?></td>
                                    <td class="text-white"><?php echo $registro['contacto']; ?></td>
                                    <td class="text-white"><?php echo $registro['email']; ?></td>
                                    <td class="text-white"><?php echo $registro['latitud']; ?></td>
                                    <td class="text-white"><?php echo $registro['longitud']; ?></td>
                                    <td class="text-white"><?php echo $registro['estado']; ?></td>
                                    
                                    <td>
                                        
                                    <form action="" method="post">


                                        <input type="hidden" name="txtid" value="<?php echo $registro['id']; ?>">
                                        <input type="hidden" name="txtnombre" value="<?php echo $registro['nombre']; ?>">
                                        <input type="hidden" name="txtdireccion" value="<?php echo $registro['direccion']; ?>">
                                        <input type="hidden" name="txtcontacto" value="<?php echo $registro['contacto']; ?>">
                                        <input type="hidden" name="txtemail" value="<?php echo $registro['email']; ?>">
                                        <input type="hidden" name="txtlatitud" value="<?php echo $registro['latitud']; ?>">
                                        <input type="hidden" name="txtlongitud" value="<?php echo $registro['longitud']; ?>">
                                        <input type="hidden" name="txtestado" value="<?php echo $registro['estado']; ?>">

                                        <input type="submit" value="Seleccionar" class="btn text-white" name="accion" style="background-color: #FF7F00;">
                                        
                                        <button value="btnEliminar" onclick="return Confirmar('¿Realmente deseas borrar el registro?');" type="submit" class="btn btn-danger" name="accion">Eliminar </button>
                                        
                                       <!-- <a type="submit" value=" ?php $txtid?> " class="btn text-white" href="personas.php?id=<?php echo $registro['id']; ?>" name="btnPersonas" style="background-color: #000000;" >Personas</a> -->

                                        <a class="btn text-white" href="#" id="arduino" onclick="arduino(<?php echo $registro['id']; ?>)" style="background-color: #000000;" >Personas</a>
                                        
                                        <!-- Button trigger modal -->
                                        <a type="button" class="btn btn-success text-white" data-toggle="modal" data-target="#ModalMap" href="#" id="mapa" onclick="mapa(<?php echo $registro['id']; ?>)">Ver mapa</a>


                                    </form>
                                    

                                    </td>

                                </tr>

                            <?php } ?>

                            </table>

                        </div>

    







                        <!-- fin-->
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>

           
            

    </section>
    
    <!-- /.content -->
<form action="" method="post" ectype="multipart/form-data">
 



 <!-- Modal -->
 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content bg-dark">
         <div class="modal-header">
             <h5 class="modal-title" id="exampleModalLabel">Registro de instituciones</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
             </button>
         </div>
         <div class="modal-body">

             <div class="form-row">
             
             <input type="hidden" name="txtid" required value="<?php echo $txtid;?>" placeholder="" id="txtid" requiere="">
             
             <div class="row align-items-start"> 
                 <div class="form-group col-md-4">
                 <label for="">Nombre:</label>
                 <input type="text" class="form-control <?php echo (isset($error['nombre']))?"is-invalid":"";?>" name="txtnombre" value="<?php echo $txtnombre;?>" placeholder="" id="txtnombre" requiere="">
                 <div class="invalid-feedback">
                 <?php echo (isset($error['nombre']))?$error['nombre']:"";?>
                 </div>
                 <br>
                 </div>

                 <div class="form-group col-md-4">
                 <label for="">Direccion:</label>
                 <input type="text" class="form-control <?php echo (isset($error['direccion']))?"is-invalid":"";?>" name="txtdireccion" value="<?php echo $txtdireccion;?>" placeholder="" id="txtdireccion" requiere="">
                 <div class="invalid-feedback">
                 <?php echo (isset($error['direccion']))?$error['direccion']:"";?>
                 </div>
                 <br>
                 </div>

                 <div class="form-group col-md-4">
                 <label for="">Contacto:</label>
                 <input type="text" class="form-control <?php echo (isset($error['contacto']))?"is-invalid":"";?>" name="txtcontacto" value="<?php echo $txtcontacto;?>" placeholder="" id="txtcontacto" requiere="">
                 <div class="invalid-feedback">
                 <?php echo (isset($error['contacto']))?$error['contacto']:"";?>
                 </div>
                 <br>
                 </div>
             </div>

             <div class="row align-items-start"> 
                 <div class="form-group col-md-12">
                 <label for="">Correo electronico:</label>
                 <input type="text" class="form-control <?php echo (isset($error['email']))?"is-invalid":"";?>" name="txtemail" value="<?php echo $txtemail;?>" placeholder="" id="txtemail" requiere="">
                 <div class="invalid-feedback">
                 <?php echo (isset($error['email']))?$error['email']:"";?>
                 </div>
                 <br>
                 </div>
                 
                 <div class="form-group col-md-6">
                 <label for="">Latitud:</label>
                 <input type="text" class="form-control <?php echo (isset($error['latitud']))?"is-invalid":"";?>" name="txtlatitud" value="<?php echo $txtlatitud;?>" placeholder="" id="txtlatitud" requiere="">
                 <div class="invalid-feedback">
                 <?php echo (isset($error['latitud']))?$error['latitud']:"";?>
                 </div>
                 <br>
                 </div>
             </div>

             <div class="form-group col-md-6">
             <label for="">Longitud:</label>
             <input type="text" class="form-control <?php echo (isset($error['longitud']))?"is-invalid":"";?>" name="txtlongitud" value="<?php echo $txtlongitud;?>" placeholder="" id="txtlongitud" requiere="">
             <div class="invalid-feedback">
                 <?php echo (isset($error['longitud']))?$error['longitud']:"";?>
                 </div>
             <br>
             </div>

             <div class="form-group col-md-12">
             <label for="">Estado:</label>
             <input type="number" class="form-control <?php echo (isset($error['estado']))?"is-invalid":"";?>" name="txtestado" value="<?php echo $txtestado;?>" placeholder="" id="txtestado" requiere="">
             <div class="invalid-feedback">
                 <?php echo (isset($error['estado']))?$error['estado']:"";?>
                 </div>
             <br>

             </div>

             </div>
         </div>
         <div class="modal-footer">
             
             <button class="btn text-white" <?php echo $accionAgregar;?> value="btnAgregar" type="submit" name="accion" style="background-color: #FF7F00;">Agregar</button>
             <button class="btn btn-warning" <?php echo $accionModificar;?> value="btnModificar" type="submit" name="accion">Modificar</button>
             <button class="btn btn-danger" onclick="return Confirmar('¿Realmente deseas borrar el registro?');" <?php echo $accionEliminar;?> value="btnEliminar" type="submit" name="accion">Eliminar</button>
             <button class="btn btn-primary" <?php echo $accionCancelar;?> value="btnCancelar" type="submit" name="accion">Cancelar</button>
         
         </div>
         </div>
     </div>
 </div>
 

<!-- MODAL DEL MAPA -->

                <div class="modal fade" id="ModalMap" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content bg-dark">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Maps</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                      
                      <div id="maps"><iframe height="500" width="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=es&amp;q='+data.latitud+','+data.longitud+'+(Mi%20nombre%20de%20egocios)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe></div>

                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                      </div>
                    </div>
                  </div>
                </div>

<!-- FIN DEL MODAL -->

</form>
  </div>
  <?php if($mostrarModal){?>

    <script>
        $('#exampleModal').modal('show');
    </script>

<?php }?>
  <script>
    function Confirmar(Mensaje){
        return (confirm(Mensaje))?true:false;

    }
    $(document).ready(function()
    {   $("#exampleModal").click(function()
        {
            //alert('hola');
            $('#exampleModal').modal('show');
        })
        //$('#exampleModal').modal('show');
    });
</script>







    
    <script>
    

     function arduino(id) {
          $("#map").empty();
            document.getElementById("total").innerHTML = 0
            document.getElementById("ingresaron").innerHTML = 0
            document.getElementById("egresaron").innerHTML = 0
      var ajax = $.ajax({
        url: "personasXinstituciones.php",
        beforeSend: function(){
                            //alert_chico('success','Espere por Favor...')
        },
        data: {id: id},
        type: "get",
                                //crossDomain : true,
        dataType: 'json',
        });
        ajax.done(function(data) 
        {
            console.log(data);  
            //resumen = data;
            document.getElementById("total").innerHTML =data.total
            document.getElementById("ingresaron").innerHTML =data.ingresaron
            document.getElementById("egresaron").innerHTML =data.egresaron;
            document.getElementById("fechaSting").innerHTML ='Estadistica de '+data.fechaSting;
            document.getElementById("posicion").innerHTML =data.posicion;
            if (data.total>=50)
            
            {
              $("#totalColor").css({"background-color": "red"});
            }
            if (data.total<50)
            
            {
              $("#totalColor").css({"background-color": "#00FF00"});
            }
                             
            return true;
        });
        ajax.fail(function( jqXHR, textStatus ) {
                                console.log(textStatus);
        });
    }

//ajax.done(function(data){}

    function mapa(id) {
          $("#map").empty();
            var ajax = $.ajax({
        url: "personasXinstituciones.php",
        beforeSend: function(){
                            //alert_chico('success','Espere por Favor...')
        },
        data: {id: id},
        type: "get",
                                //crossDomain : true,
        dataType: 'json',
        });
        ajax.done(function(data) 
        {
            console.log(data);  
            //resumen = data;
            
            //stringMap = '<iframe src="https://maps.google.com/?ll='+data.latitud+','+data.longitud+'&z=14&t=m&output=embed" height="200" width="465" frameborder="10" style="border:0" allowfullscreen></iframe>';


                stringMap = '<iframe height="500" width="465" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=es&amp;q='+data.latitud+','+data.longitud+'+(Mi%20nombre%20de%20egocios)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe>';


            $("#maps").html(stringMap);
             console.log(stringMap);                   
                               
            return true;
        });
        ajax.fail(function( jqXHR, textStatus ) {
                                console.log(textStatus);
        });
    }



    


    </script>




<?php include('footer.php');?>